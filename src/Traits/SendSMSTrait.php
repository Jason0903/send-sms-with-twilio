<?php
namespace CustomTwilio\SendSMS\Traits;

trait SendSMSTrait
{
    protected static function resolveName($methodName)
    {
        return app()[$methodName];
    }

    public static function __callStatic($methodName, $arguments)
    {
        // TODO: Implement __callStatic() method.
        return (self::resolveName($methodName)->$methodName(...$arguments));
    }
}