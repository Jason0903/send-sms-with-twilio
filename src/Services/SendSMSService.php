<?php

namespace CustomTwilio\SendSMS\Services;

use Twilio\Rest\Client;

class SendSMSService extends Client
{
    /**
     * @var mixed
     */
    private $twilioPhoneNumber;

    public function __construct(string $username = null, string $password = null, string $accountSid = null, string $region = null, \Twilio\Http\Client $httpClient = null, array $environment = null, array $userAgentExtensions = null)
    {
        parent::__construct();
        $this->accountSid = config('services.twilio')['TWILIO_ACCOUNT_SID'];
        $this->password = config('services.twilio')['TWILIO_AUTH_TOKEN'];
        $this->twilioPhoneNumber = config('services.twilio')['TWILIO_PHONE_NUMBER'];
    }

    public function send($toNumber, $message = '')
    {
        try {
            return $this->messages->create($toNumber, [
                'from' => $this->twilioPhoneNumber,
                'body' => $message
            ]);
        } catch (\Twilio\Exceptions\TwilioException $e) {
            \Illuminate\Support\Facades\Log::error($e);
        }
    }
}