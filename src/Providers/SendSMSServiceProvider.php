<?php

namespace CustomTwilio\SendSMS\Providers;

use CustomTwilio\SendSMS\Services\SendSMSService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class SendSMSServiceProvider extends ServiceProvider
{

    public function register()
    {
    }

    public function boot()
    {
        app()->singleton('send',function (){
            return new SendSMSService();
        });

    }
}