<?php

namespace CustomTwilio\SendSMS\Classes;

use CustomTwilio\SendSMS\Traits\SendSMSTrait;

class SendSMS
{
    use SendSMSTrait;
    protected static $methodName = 'send';
}